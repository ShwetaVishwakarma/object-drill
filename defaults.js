module.exports.defaultObject = (obj, obj1) => {
    for (let k in obj) {
        if (obj[k] === undefined) {
            obj[k] = obj1[k]
        }
    }
    return obj
}